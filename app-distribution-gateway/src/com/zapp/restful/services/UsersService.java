package com.zapp.restful.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;


@Path("/")
public class UsersService {
	
	private static String emailId, group;
	private String[] emailLists;
	//private Array appIds;
	
	//{"emailId":"abc@xyz.com","group":"group"}
	
	@GET
	@Path("/testers/emailId")
	@Produces("text/plain")
	public static String getEmailId() {
		return emailId;
	}

	@POST
	@Path("/testers/emailId")
	@Produces("text/plain")
	public static void setEmailId(String emailId) {
		UsersService.emailId = emailId;
	}

	@GET
	@Path("/testers/group")
	@Produces("text/plain")
	public static String getGroup() {
		return group;
	}

	@POST
	@Path("/testers/group")
	@Produces("text/plain")
	public static void setGroup(String group) {
		UsersService.group = group;
	}

	@GET
	@Path("/testers/getTesters")
	@Produces("application/json")
	public String getTesters() 
	{		
		List<String> emailList = new ArrayList();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://Localhost:3306/app_distribution","root","changeit");
			PreparedStatement ps = con.prepareStatement("select * from testers");
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				System.out.format("Result -- %s",rs.getString("emailId"));
				emailList.add(rs.getString("emailId"));
				}
		      } catch (Exception e){
			System.out.println(e);
		}
		String pattern = "{\"emailId\":\"%s\"}";
		return String.format(pattern, emailList);
	}
	
	//localhost:8080/app-distribution-gateway/testers?emailId=abc@xyz.com
	@PUT
	@Path("/testers/addTester")
	@Produces("application/json")
	public String addTester(@QueryParam("emailId") String emailId)
	{
		UsersService.emailId = emailId;
		//UsersService.group = group;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://Localhost:3306/app_distribution","root","changeit");
			PreparedStatement ps = con.prepareStatement("insert into testers(emailId) values(?)");
			ps.setNString(1, emailId);
			int i = ps.executeUpdate();
			if(i > 0) {
				System.out.println("Data inserted successfully...!");
			}else {
				System.out.println("Data insertion failed...!");
			}
		} catch (Exception e){
			System.out.println(e);
		}
		
		String pattern = "{\"emailId\":\"%s\"}";
		return String.format(pattern, emailId);
	}
		
	
	@POST
	@Path("/testers/uploadBuild")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public String uploadImage(@FormDataParam("file") InputStream uploadedInputStream,
							  @FormDataParam("file") FormDataContentDisposition fileDetails) {

	   System.out.println(fileDetails.getFileName());

	   String uploadedFileLocation = "/Users/temp/" + fileDetails.getFileName();

	   // save it
	   writeToFile(uploadedInputStream, uploadedFileLocation);

//	   String output = "File uploaded to : " + uploadedFileLocation;
//
//	   ResponseBean responseBean = new ResponseBean();
//
//	   responseBean.setCode(StatusConstants.SUCCESS_CODE);
//	   responseBean.setMessage(fileDetails.getFileName());
//	   responseBean.setResult(null);
	   return "Build uploaded successfully...!";
	}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
	                   String uploadedFileLocation) {
	   try {
	      OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
	      int read = 0;
	      byte[] bytes = new byte[1024];

	      out = new FileOutputStream(new File(uploadedFileLocation));
	      while ((read = uploadedInputStream.read(bytes)) != -1) {
	         out.write(bytes, 0, read);
	      }
	      out.flush();
	      out.close();
	   } catch (IOException e) {
	      e.printStackTrace();
	   }
	}
	
}
